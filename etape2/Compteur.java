
public class Compteur {

	private SharedObject entier;
	private int nb;
	
	public Compteur(SharedObject s, int nb) {
		this.entier = s;
		this.nb = nb;
	}

	public static void main(String[] argv) {
		Client.init();
		SharedObject s = Client.lookup("ENTIER");
		if (s == null) {
			System.out.println("s est null :(");
			s = Client.create(new Entier());
			Client.register("ENTIER", s);
			System.out.println("Entier registered to the rmiregistry");
		}
		
		Compteur c = new Compteur(s, new Integer(argv[0]));
		
		c.run();
		
	}

	private void run() {
		Entier e;
		for(int i=0; i<nb; i++) {
			
			entier.lock_write();
			e = (Entier) (entier.obj);
			e.incrementer();
			entier.unlock();
			
			entier.lock_read();
			try {
				Thread.sleep((long) 0.1);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			System.out.println(e.getEntier());
			entier.unlock();
		}
		
	}
	
}
