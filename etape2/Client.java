import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client extends UnicastRemoteObject implements Client_itf {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5744024746331630660L;

	private static Server_itf serveur;

	public static String adresse;

	public static String URL;

	public static Client_itf instance;

	public static String nom;

	public static HashMap<Integer, SharedObject> annuaire_so = new HashMap<Integer, SharedObject>();;

	public static Logger logger = Logger.getLogger(Client.class.getName());

	public Client() throws RemoteException {
		super();
	}

	// /////////////////////////////////////////////////
	// Interface to be used by applications //
	// /////////////////////////////////////////////////

	// initialization of the client layer
	public static void init() {
		logger.setLevel(Level.ALL);

		// On initialise le stub Client -> Server
		logger.log(Level.INFO, "Initialisation du client");

		try {
			// adresse = InetAddress.getLocalHost().getHostName();
			adresse = "localhost";

			// recuperation d'un stub sur l'objet serveur
			serveur = (Server_itf) Naming.lookup("//" + adresse + ":" + 1099
					+ "/mon_serveur");
			logger.log(Level.INFO, "Le client est connecté au serveur");

			instance = new Client();
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			e.printStackTrace();
		}
	}

	// lookup in the name server
	public synchronized static SharedObject lookup(String name) {

		// System.out.println("Lookup de " + name + " ...");
		SharedObject res = null;
		nom = name;
		try {
			SharedObject so = null;
			int i = serveur.lookup(name);
			// System.out.println("i a pour valeur " + i);
			if (i != -1) {
				// System.out.println("trouvé dans le serveur donc inséré dans annuaire_so");
				so = (SharedObject) StubGenerator.generateStub(
						serveur.getInstanceClass(i), i);
				annuaire_so.put(i, so);
			}
			return so;
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return res;
	}

	// binding in the name server
	public synchronized static void register(String name, SharedObject_itf so) {
		// System.out.println("Register de " + name);
		try {
			serveur.register(name, ((SharedObject) so).getId());
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	// creation of a shared object
	public synchronized static SharedObject create(Object o) {
		// On appelle le serveur pour creer l'id de l'object

		SharedObject so = null;
		try {
			int i = serveur.create(o);
			// so = new SharedObject(o,i);
			so = (SharedObject) StubGenerator.generateStub(o, i);
			annuaire_so.put(i, so);
		} catch (RemoteException e) {
			e.printStackTrace();
		}

		return so;
	}

	// //////////////////////////////////////////////////////////
	// Interface to be used by the consistency protocol //
	// //////////////////////////////////////////////////////////

	// request a read lock from the server
	public static Object lock_read(int id) {
		logger.log(Level.INFO, "Le Client " + URL
				+ " demande lock_read au serveur sur le ServerObject " + id);

		try {
			return serveur.lock_read(id, instance);
		} catch (RemoteException e) {
			e.printStackTrace();
			return null;
		}
	}

	// request a write lock from the server
	public static Object lock_write(int id) {
		logger.log(Level.INFO, "Le Client " + URL
				+ " demande lock_write au serveur sur le ServerObject " + id);
		try {
			return serveur.lock_write(id, instance);
		} catch (RemoteException e) {
			e.printStackTrace();
			return null;
		}
	}

	// receive a lock reduction request from the server
	public Object reduce_lock(int id) throws java.rmi.RemoteException {
		logger.log(Level.INFO, "Le Client " + URL
				+ " demande reduce_lock au SharedObject " + id);

		return annuaire_so.get(id).reduce_lock();

	}

	// receive a reader invalidation request from the server
	public void invalidate_reader(int id) throws java.rmi.RemoteException {
		logger.log(Level.INFO, "Le Client" + adresse
				+ " demande invalidate_reader au SharedObject " + id);

		annuaire_so.get(id).invalidate_reader();
	}

	// receive a writer invalidation request from the server
	public Object invalidate_writer(int id) throws java.rmi.RemoteException {
		logger.log(Level.INFO, "Le Client" + adresse
				+ " demande invalidate_writer au SharedObject " + id);

		return annuaire_so.get(id).invalidate_writer();

	}
}
