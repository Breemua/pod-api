import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

public class StubGenerator {
	public static SharedObject_itf generateStub(Object object, int id) {

		String name;
		System.out.println(name = generateJavaFile(object));
		compile(name);
		try {
			Class<?> classe = Class.forName(name + "_stub");

			Class<?>[] types = new Class[] { Object.class, int.class };
			Constructor<?> constructeur = classe.getConstructor(types);
			Object[] args = new Object[] { object, id };

			return (SharedObject) constructeur.newInstance(args);

		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}

		return null;
	}

	//
	// public static void main(String[] argv) {
	// generateStub(new Trolol<String>("Megalol"), 42);
	// }

	private static int compile(String name) {
		File file = new File(name + "_stub.java");
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		StandardJavaFileManager fileManager = compiler.getStandardFileManager(
				null, null, null);
		Iterable<? extends JavaFileObject> compilationUnits = fileManager
				.getJavaFileObjectsFromFiles(Arrays.asList(file));
		compiler.getTask(null, fileManager, null, null, null, compilationUnits)
				.call();
		return 0;
	}

	private static String generateJavaFile(Object object) {

		StringBuilder res = new StringBuilder();

		Class<? extends Object> o = object.getClass();

		String className = o.getName();

		Method[] methodes = o.getMethods();

		res.append("public class ").append(className).append("_stub ")
				.append("extends SharedObject ").append("implements ")
				.append(className).append("_itf, java.io.Serializable {\n\n");

		res.append("public ").append(className)
				.append("_stub(Object obj, int id) {\n")
				.append("super(obj, id);\n").append("}\n\n");

		String returnType;
		String methodeName;
		Class<?>[] exceptions;
		for (Method methode : methodes) {
			returnType = methode.getReturnType().getName();
			methodeName = methode.getName();
			int mod = methode.getModifiers();

			if (Modifier.isAbstract(mod)
					|| Modifier.isPrivate(mod)
					|| Modifier.isProtected(mod)
					|| Modifier.isInterface(mod)
					|| Modifier.isNative(mod)
					|| methode.getDeclaringClass().getName()
							.equals("java.lang.Object")) {
				// On n'en tient pas compte car on ne peut pas les appeler !
			} else {
				if (Modifier.isFinal(mod)) {
					res.append("final ");
				}
				if (Modifier.isPublic(mod)) {
					res.append("public ");
				}
				if (Modifier.isStatic(mod)) {
					res.append("static ");
				}
				if (Modifier.isStrict(mod)) {
					res.append("strict ");
				}
				if (Modifier.isSynchronized(mod)) {
					res.append("synchronized ");
				}
				if (Modifier.isTransient(mod)) {
					res.append("transient ");
				}
				if (Modifier.isVolatile(mod)) {
					res.append("volatile ");
				}

				res.append(returnType).append(" ");
				res.append(methodeName).append("(");
				Class<?>[] arguments = methode.getParameterTypes();
				Class<?> argument;
				int i = 0;
				if (arguments.length > 0) {
					for (i = 0; i < arguments.length - 1; i++) {
						argument = arguments[i];
						res.append(argument.getName()).append(" arg_")
								.append(i).append(", ");
					}
					res.append(arguments[i].getName()).append(" arg_")
							.append(i);
				}
				res.append(")");

				exceptions = methode.getExceptionTypes();
				if (exceptions.length > 0) {
					Class<?> exception;

					res.append(" throws ");
					int k = 0;
					for (k = 0; k < exceptions.length - 1; k++) {
						exception = exceptions[k];
						res.append(exception.getName()).append(", ");
					}
					res.append(exceptions[k].getName()).append(" ");
				}

				res.append(" {\n");

				res.append(className).append(" s = (").append(className)
						.append(") obj;\n");
				if (!returnType.equals("void")) {
					res.append("return ");
				}
				res.append("s.").append(methodeName).append("(");
				if (arguments.length > 0) {
					int j = 0;
					for (j = 0; j < arguments.length - 1; j++) {
						res.append("arg_").append(j).append(", ");
					}
					res.append("arg_").append(j);
				}
				res.append(");\n");

				res.append("}\n\n");

			}
		}

		res.append("}\n");

		try {
			FileWriter fstream = new FileWriter(className + "_stub.java");
			BufferedWriter out = new BufferedWriter(fstream);
			out.write(res.toString());

			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return className;

	}
}
