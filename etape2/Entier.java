import java.io.Serializable;


public class Entier implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8659221129903040477L;
	
	private int entier;
	
	public Entier() {
		entier = 0;
	}
	
	public void incrementer() {
		entier++;
	}
	
	public int getEntier() {
		return this.entier;
	}
}
