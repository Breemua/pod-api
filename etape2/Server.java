import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server extends UnicastRemoteObject implements Server_itf {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8081742325119625054L;

	/**
	 * 
	 */
	public static int port;
	public static String adresse;
	public static HashMap<String, Integer> annuaire = new HashMap<String, Integer>();
	public static HashMap<Integer, ServerObject> annuaire_obj = new HashMap<Integer, ServerObject>(); // associe
																										// un
																										// ServerObject
																										// a
																										// un
																										// id
	private static int compteur = 0;

	/**
	 * Permet d'être sur que plusieurs clients ne fassent pas de lookup/register
	 * en même temps comportement rendu possible par la division du code
	 * d'initialisation d'un client.
	 */
	private static Lock lock = new ReentrantLock();

	private static Logger logger = Logger.getLogger(Server.class.getName());

	/**
	 * Construire un Server en specifiant un numero de port
	 * 
	 * @param port
	 *            un numero de port
	 * @throws RemoteException
	 */
	public Server(int port) throws RemoteException {
		super();
		Server.port = port;
		Server.adresse = "localhost";

		logger.setLevel(Level.ALL);

	}

	@Override
	/**
	 * Chercher l'ID d'un objet a partir de son nom
	 * @param name le nom de l'objet
	 * @return l'ID de l'objet s'il existe et null sinon
	 * @throws RemoteException
	 */
	public int lookup(String name) throws RemoteException {
		lock.lock();
		if (annuaire.containsKey(name)) {
			lock.unlock();
			return annuaire.get(name);
		} else {
			return -1;
		}
	}

	/**
	 * Enregistrer une entree dans le serveur de noms
	 * 
	 * @param name
	 *            le nom de l'objet a enregistrer
	 * @param id
	 *            l'identifiant de l'objet a enregistrer
	 * @throws RemoteException
	 */
	@Override
	public void register(String name, int id) throws RemoteException {

		annuaire.put(name, id);
		lock.unlock();

	}

	/**
	 * Creer un objet partage (SharedObject) a partir d'un objet. L'etat initial
	 * de l'objet partage est NL Un identifiant unique est attribue a l'objet
	 * partage. Un objet ServerObject est cree avec le meme ID que celui de
	 * l'objet partage.
	 * 
	 * @param o
	 *            l'objet qui va etre partage
	 * @return l'ID qui identifie l'objet
	 * @throws RemoteException
	 */
	@Override
	public int create(Object o) throws RemoteException {
		int identifiant = Server.getId();
		ServerObject objServ = new ServerObject(identifiant, o);
		Server.annuaire_obj.put(identifiant, objServ);
		return identifiant;
	}

	/**
	 * Transmet la requete lock_read a au ServerObject qui a pour identifiant id
	 * 
	 * @param id
	 *            l'identifiant de l'objet partage qui demande a lire
	 * @param les
	 *            information du client qui a appele la methode
	 * @return l'objet souhaite
	 */
	@Override
	public Object lock_read(int id, Client_itf client) throws RemoteException {
		logger.log(Level.INFO, "Server " + adresse
				+ " demande lock_read au ServerObject " + id);
		ServerObject so = Server.annuaire_obj.get(id);
		if (so != null) {
			return so.lock_read(client);
		} else {
			return null;
		}
	}

	/**
	 * Transmet la requete lock_write a au ServerObject qui a pour identifiant
	 * id
	 * 
	 * @param id
	 *            l'identifiant de l'objet partage qui demande a ecrire
	 * @param les
	 *            information du client qui a appele la methode
	 * @return l'objet souhaite
	 */
	@Override
	public Object lock_write(int id, Client_itf client) throws RemoteException {
		logger.log(Level.INFO, "Le Server " + adresse
				+ " demande lock_write au ServerObject " + id);
		ServerObject so = Server.annuaire_obj.get(id);
		if (so != null) {
			return so.lock_write(client);
		} else {
			return null;
		}
	}

	/**
	 * Obtenir un identifiant unique
	 * 
	 * @return un indentifiant unique
	 */
	public static synchronized int getId() {
		Server.compteur++;
		return Server.compteur;
	}

	public Object getInstanceClass(int id) {
		return annuaire_obj.get(id).obj;
	}

	public static synchronized Object reduce_lock(Client_itf client, int id) {
		logger.log(Level.INFO, "Le Server " + adresse
				+ " demande reduce_lock au Client sur le SharedObject " + id);
		Object obj = null;
		try {
			obj = client.reduce_lock(id);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return obj;
	}

	public static synchronized void invalidate_reader(Client_itf client, int id) {
		logger.log(Level.INFO, "Le Server " + adresse
				+ " demande invalidate_reader au Client sur le SharedObject "
				+ id);
		try {
			client.invalidate_reader(id);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	public static synchronized Object invalidate_writer(Client_itf client,
			int id) {
		logger.log(Level.INFO, "Le Server " + adresse
				+ " demande invalidate_writer au Client sur le SharedObject "
				+ id);
		Object obj = null;
		try {
			obj = client.invalidate_writer(id);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return obj;
	}
}
