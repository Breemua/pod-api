import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Pour lancer un serveur
 */
public class StartServer {

	public static Logger logger = Logger.getLogger(StartServer.class.getName());
	
	public static void main(String[] args) {
		logger.setLevel(Level.ALL);
		int port = 1099;
		try {
			// creation d'un objet Server
			Server serv = new Server(port);
			// creation du rmiregistry
			LocateRegistry.createRegistry(port);
			// enregistrement de l'objet cree dans le serveur de noms
			// Naming.rebind("//" + serv.adresse + ":" + serv.port + "/mon_serveur",serv);
			Naming.rebind("//localhost:" + port + "/mon_serveur",serv);
			
			logger.log(Level.INFO, "Le serveur est prêt");
		} catch (RemoteException | MalformedURLException e) {
			logger.log(Level.SEVERE, "Erreur pendant la création du serveur");
			e.printStackTrace();
		}

	}

}
