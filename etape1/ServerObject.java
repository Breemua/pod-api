import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ServerObject {

    private enum Lock {
    	NL,
	    RL,
	    WL
	    }
	
	
	Lock lock;
	Collection<Client_itf> lecteurs;
	Client_itf redacteur;
	Object obj;

	private static Logger logger = Logger.getLogger(ServerObject.class.getName());
	
	
	/**
	 * Correspond à l'id du Shared Object qu'il "contrôle"	
	 */
	int id;
	
	public ServerObject(int identifiant, Object o) {
		this.lock = Lock.NL;
		this.id = identifiant;
		this.lecteurs = new ArrayList<Client_itf>();
		this.redacteur = null;
		this.obj = o;
		
		logger.setLevel(Level.ALL);
		
	}
	
	public synchronized Object lock_read(Client_itf client) {
		logger.log(Level.INFO, "Le ServerObject " + id + " applique lock_read");
		switch(lock) {
		case NL:
			// le client peut lire
			this.lock = Lock.RL;
			logger.log(Level.INFO, "Le ServerObject " + this.id + " passe en RL");
			this.lecteurs.add(client); // client devient lecteur
			break;
		case RL:
			// le client peut lire
			this.lecteurs.add(client); // client devient lecteur
			logger.log(Level.INFO, "Le ServerObject " + this.id + " reste en RL");
			break;
		case WL:
			// il faut invalider l'ecrivain en appelant reduce_lock
			logger.log(Level.INFO, "Le ServerObject " + this.id + " demande au serveur d'invalider le rédacteur");
			this.obj = Server.reduce_lock(this.redacteur,this.id);
			this.lecteurs.add(this.redacteur);
			this.redacteur = null;
			if (this.obj instanceof Sentence) {
				Sentence s = (Sentence) this.obj;
				logger.log(Level.INFO, "Ce qu'il y a ds l'objet à jour : " + s.data);
			}
			this.lock = Lock.RL;
			this.lecteurs.add(client); // client devient lecteur
			logger.log(Level.INFO, "Le ServerObject " + this.id + " passe en RL");
			break;
		default:
			break;	
		}
		return this.obj;
	}
	
	public synchronized Object lock_write(Client_itf client) {
		logger.log(Level.INFO, "Le ServerObject " + id + " applique lock_write");
		switch(lock) {
		case NL:
			// le client peut ecrire
			this.lock = Lock.WL;
			this.redacteur = client; // client devient redacteur
			logger.log(Level.INFO, "Le ServerObject " + this.id + " passe en WL");
			break;
		case RL:
			// il faut invalider tous les lecteurs
			logger.log(Level.INFO, "Le ServerObject " + this.id + " demande au serveur d'invalider tous les lecteurs");
			for (Client_itf c : this.lecteurs) {
				if (!c.equals(client)) {
					Server.invalidate_reader(c ,this.id);
				}
			}
			this.lecteurs.clear();
			this.lock = Lock.WL;
			logger.log(Level.INFO, "Le ServerObject " + this.id + " passe en WL");
			this.redacteur = client;
			break;
		case WL:
			// il faut invalider le redacteur
			logger.log(Level.INFO, "Le ServerObject " + this.id + " demande au serveur d'invalider le rédacteur");
			this.obj = Server.invalidate_writer(this.redacteur,this.id); // mise a jour de l'objet partage
			this.redacteur = client;
			logger.log(Level.INFO, "Le ServerObject " + this.id + " reste en WL");
			break;
		default:
			break;	
		}
		return this.obj;
	}
	
	
	
}
