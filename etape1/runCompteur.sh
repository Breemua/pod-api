#!/bin/bash

if [ $# != 2 ]; 
then echo " On lance le test comme cela : \n "
    echo " ./runCompteur.sh <nombre de clients> <taille des boucle dans les clients>"
else

    process="";

    for (( i=1; i<=$1; i++ ))
    do
	echo "Lancement de Compteur n° " + $i;
	java Compteur $2 &
	process=$process" "$!
    done

    echo $process;
  
    
fi
