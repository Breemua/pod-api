import java.io.Serializable;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SharedObject implements Serializable, SharedObject_itf {
	
    /**
     * 
     */
    private static final long serialVersionUID = 2135203967525495035L;


    private enum Lock {
    	NL,
	    RLC,
	    WLC,
	    RLT,
	    WLT,
	    RLT_WLC
	    }
	
    private Lock lock;
	
    private ReentrantLock invalidateLock;
    private Condition invalidateCondition;

    public Object obj;
    
    private int id;
	
    private static Logger logger = Logger.getLogger(SharedObject.class.getName());
	
    public SharedObject(Object obj, int id) {
	this.lock = Lock.NL;
	this.obj = obj;
	this.id = id;
	
	this.invalidateLock = new ReentrantLock();
	this.invalidateCondition = invalidateLock.newCondition();

	
	logger.setLevel(Level.ALL);
	
	ConsoleHandler ch=new ConsoleHandler();
	logger.addHandler(ch);
	ch.setLevel(Level.ALL);
	
    }
	
    public int getId() {
	return this.id;
    }
	
    // invoked by the user program on the client node
    public synchronized void lock_read() {
	/*
	 *  NL -> Appel au serveur ... puis à terme RLT
	 *  RLC -> RLT
	 *  WLC -> RLT_WLC
	 *  RLT -> interdit
	 *  WLT -> interdit
	 *  RLT_WLC -> interdit
	 */
		
	switch(lock) {
		
	case NL:
	    obj = Client.lock_read(id);
	    this.lock = Lock.RLT;
	    break;
			
	case RLC:
	    this.lock = Lock.RLT;
	    break;
			
	case WLC:
	    this.lock = Lock.RLT_WLC;
	    break;
			
	default:
	    logger.log(Level.SEVERE, "################################lock_read interdit, etat actuel " + lock);
	    break;
				
	}
		
    }

    // invoked by the user program on the client node
    public synchronized void lock_write() {
	/*
	 *  NL -> Appel au serveur ... puis à terme WLT
	 *  RLC -> Appel au serveur ... puis à terme WLT
	 *  WLC -> WLT
	 *  RLT -> interdit
	 *  WLT -> interdit
	 *  RLT_WLC -> interdit
	 */
		
		
	switch(lock) {
		
	case NL:

	    this.obj = Client.lock_write(id);
	    this.lock = Lock.WLT;
	    break;
			
	case RLC:
	    obj = Client.lock_write(id);
	    this.lock = Lock.WLT;
	    break;
		
	case WLC:
	    this.lock = Lock.WLT;
	    break;
			
	default:
	    logger.log(Level.SEVERE, "################################lock_write interdit, etat actuel " + lock);
	    break;
	}
		
    }

    // invoked by the user program on the client node
    public synchronized void unlock() {

	/*
	 *  NL -> interdit
	 *  RLC -> interdit
	 *  WLC -> interdit
	 *  RLT -> RLC
	 *  WLT -> WLC
	 *  RLT_WLC -> WLC
	 */
		
		
	switch(lock) {
		
	
	case RLT:
	    this.lock = Lock.RLC;
	    break;
		
	case WLT:
	    this.lock = Lock.WLC;
	    break;
		
	case RLT_WLC:
	    this.lock = Lock.WLC;
	    break;
		
	default:
	    logger.log(Level.SEVERE, "################################unlock interdit, etat actuel " + lock);
	    break;
	}
		
	notify();
	
	invalidateLock.lock();
	invalidateCondition.signal();
	invalidateLock.unlock();
	
    }


    // callback invoked remotely by the server
    public synchronized Object reduce_lock() {
	/*
	 *  NL -> interdit
	 *  RLC -> interdit
	 *  WLC -> RLC
	 *  RLT -> interdit
	 *  WLT -> RLC
	 *  RLT_WLC -> RLT
	 */

	
	//	logger.log(Level.INFO, "-----------------reduce_lock() " + lock);

				
	switch(lock) {
		
	case WLC:
	    this.lock = Lock.RLC;
	    break;
		
	case WLT:
	    while(lock == Lock.WLT) {
		try {
		    wait();
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}
	    }
	    this.lock = Lock.RLC;
	    break;
		
	case RLT_WLC:
	    this.lock = Lock.RLT;
	    break;
			
	default:
	    logger.log(Level.SEVERE, "################################reduce_lock interdit, etat actuel " + lock);
	    break;
	}
		
	return this.obj;
		
    }

    // callback invoked remotely by the server
    public void invalidate_reader() {
	/*
	 *  NL -> NL
	 *  RLC -> NL
	 *  WLC -> interdit
	 *  RLT -> NL
	 *  WLT -> interdit
	 *  RLT_WLC -> interdit
	 */
		

	//	logger.log(Level.INFO, "-----------------invalidate_reader() " + lock);

	invalidateLock.lock();
    	
	switch(lock) {
			
	case RLC:
	    this.lock = Lock.NL;
	    break;
			
	case RLT:
	    while(lock == Lock.RLT) {
		try {
		    invalidateCondition.await();
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}
	    }
	    this.lock = Lock.NL;
	    break;
			
	default:
	    logger.log(Level.SEVERE, "################################invalidate_reader interdit, etat actuel " + lock);
	    break;
	}
	
	invalidateLock.unlock();   

}
    
   

	
    public synchronized Object invalidate_writer() {
	/*
	 *  NL -> NL
	 *  RLC -> interdit
	 *  WLC -> NL
	 *  RLT -> interdit 
	 *  WLT -> NL apres un unlock()
	 *  RLT_WLC -> NL apres un unlock().
	 */


	//	logger.log(Level.INFO, "-----------------invalidate_writer() " + lock);
		
	switch(lock) {
	
	case WLC:
	    this.lock = Lock.NL;
	    break;
			
	case WLT:
	case RLT_WLC:
	    while(lock == Lock.RLT_WLC || lock == Lock.WLT) {
		try {
		    wait();
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}
	    }
	    this.lock = Lock.NL;
	    break;
			
	default:
	    logger.log(Level.SEVERE, "################################invalidate_writer interdit, etat actuel " + lock);
	    break;
	}
		
	return this.obj;

    }
}
